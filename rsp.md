```python
from mpi4py import MPI
import veloxchem as vlx
import numpy as np
import sys

molecule_string = """
    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037"""

basis_set_label = '6-31G'

scf_settings = {'conv_thresh': 1.0e-6}
method_settings = {'xcfun': 'b3lyp', 'grid_level': 4}
rsp_settings = {'conv_thresh': 1.0e-4, 'nstates': 5}

# molecule and basis set
molecule = vlx.Molecule.read_str(molecule_string, units='angs')
basis = vlx.MolecularBasis.read(molecule, basis_set_label)

# communicator and output stream
comm = MPI.COMM_WORLD
ostream = vlx.OutputStream(sys.stdout)

# print molecular geometry and basis set information
ostream.print_block(molecule.get_string())
ostream.print_block(basis.get_string('Atomic Basis', molecule))

# run SCF
scfdrv = vlx.ScfRestrictedDriver(comm, ostream)
scfdrv.update_settings(scf_settings, method_settings)
scfdrv.compute(molecule, basis)

# run linear response
lr_eig_drv = vlx.LinearResponseEigenSolver(comm, ostream)
lr_eig_drv.update_settings(rsp_settings, method_settings)
lr_results = lr_eig_drv.compute(molecule, basis, scfdrv.scf_tensors)
lr_eig_drv.ostream.flush()

if lr_eig_drv.is_converged:
    print('Excitation energies:\n')
    for i, w in enumerate(lr_results['eigenvalues']):
        print('  State {:2d}:   {:.5f} eV'.format(i+1, w * vlx.hartree_in_ev()))

print('\nComputing E[2] matrix:\n')
e2 = lr_eig_drv.get_e2(molecule, basis, scfdrv.scf_tensors)
e2 /= 2.0
print('\nSize of E[2] matrix: ({},{})'.format(*e2.shape))
```