```python
import MDAnalysis as mda
import nglview as nv
from nglview.datafiles import PDB, XTC

u = mda.Universe(PDB, XTC)

protein = u.select_atoms('protein')

w = nv.show_mdanalysis(protein)
w
```