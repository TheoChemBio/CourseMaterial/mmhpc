```
@jobs
task: adc
@end

@method settings
basis: def2-SVP
@end
	
@adc
method: cvs-adc2
singlets: 10
core_orbitals: 2-5
@end
	
@molecule
charge: 0
multiplicity: 1
units: angstrom
xyz:
S             -0.000000    1.192685    0.000000
C             -0.711444   -1.266809    0.000000
C              0.711447   -1.266803   -0.000000
C             -1.238145   -0.009743    0.000000
C              1.238143   -0.009741   -0.000000
H             -1.314338   -2.162700    0.000000
H              1.314340   -2.162697   -0.000000
H             -2.274762    0.280500    0.000000
H              2.274758    0.280506   -0.000000
@end
```
