```bash
#!/bin/bash

#SBATCH -A edu20.mmhpc
#SBATCH -C Haswell
#SBATCH --nodes 1
#SBATCH --ntasks-per-node 1
#SBATCH --time 1:00:00
#SBATCH --job-name myjob

module load anaconda/py36/5.0.1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/pdc/vol/anaconda/co7/5.0.1/py36_dep/openblas/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib64
export OMP_NUM_THREADS=24

mpirun -n 1 --bind-to none python3 -m gator myjob.inp myjob.out
```