# Molecular Modeling on HPC Systems

## Login to PDC's machines

+ [How to login from Linux](https://www.pdc.kth.se/support/documents/login/linux_login.html)

+ [How to login from Windows](https://www.pdc.kth.se/support/documents/login/windows_login.html)

+ [How to login from MacOS](https://www.pdc.kth.se/support/documents/login/mac_login.html)

## Run jobs in Jupyter notebook

+ [General instruction on how to use Jupyter notebooks](https://www.pdc.kth.se/software/software/Jupyter-Notebooks/centos7/5.0.0/index_using.html)

+ [Submission script for running Jupyter notebook on a compute node of Tegner](jupyter.md)

+ [Running SCF calculation in Jupyter notebook](notebooks/nb-scf.ipynb)

+ [Running geometry optimization in Jupyter notebook](notebooks/nb-geom-opt.ipynb)

+ [Running response calculation in Jupyter notebook](notebooks/nb-rsp.ipynb)

+ [Running MP2 calculation in Jupyter notebook](notebooks/nb-mp2.ipynb)

+ [Running ADC calculation in Jupyter notebook](notebooks/nb-adc.ipynb)

+ [Running CVS-ADC calculation in Jupyter notebook (thiophene example)](notebooks/cvs_adc_thiophene_example.ipynb)

+ [Running nglview in Jupyter notebook](nglview.md)

## Submit jobs via batch script

+ Running VeloxChem on Tegner

  - [Submission script for running VeloxChem on Tegner](sbatch_vlx_tegner.md)

  - [Input file for geometry optimization](geom_opt_input.md)

  - Input file for [scanning coordinate](geom_scan_input.md) and the [constraints](geom_scan_constraints.md)

+ Running GATOR on Tegner

  - [Submission script for running GATOR on Tegner](sbatch_gator_tegner.md)

  - [Input file for MP2 calculation](mp2_input.md)

  - [Input file for adc calculation](gator_input_adc.md) and corresponding [submit script](sbatch_gator_adc.md)

+ Running GROMACS on Tegner

  - [Submission script for running GROMACS on Tegner](sbatch_gromacs_tegner.md)

## Files for the exercises

+ [Experimental C K-edge XAS spectrum of vinylfluoride](vinylfluoride.dat)

+ [Xyz file for the thiophene molecule used in the cvs-adc jupyter notebook example](thiophene.xyz)

+ [Files for MD exercices](MD.tar.gz)

+ [generator.py for QM/MM exercise](generator.py) 

## Slides

+ Response theory

  - [exact_response.pdf](slides/exact_response.pdf)
  
  - [approx_response.pdf](slides/approx_response.pdf)
