```bash
#!/bin/bash

#SBATCH -A edu20.mmhpc
#SBATCH -C Haswell
#SBATCH --nodes 1
#SBATCH --ntasks-per-node 1
#SBATCH --time 1:00:00
#SBATCH --job-name myjob

module load anaconda/py36/5.0.1
export OMP_NUM_THREADS=24

mpirun -n 1 --bind-to none python3 -m veloxchem myjob.inp myjob.out
```