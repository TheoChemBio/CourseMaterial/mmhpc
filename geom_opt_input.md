```
@jobs
task: optimize
@end

@method settings
xcfun: b3lyp
grid_level: 4
basis: 6-31g
@end

@optimize
coordsys: tric
@end

@molecule
charge: 0
multiplicity: 1
xyz:
O 0 0 0
H 0 0 1.795239827225189
H 1.693194615993441 0 -0.599043184453037
@end
```