```bash
#!/bin/bash

#SBATCH -N 1
#SBATCH -t 2:00:00
#SBATCH -J frame1
#SBATCH -A edu20.mmhpc
#SBATCH -C Haswell
#SBATCH -e error.log
#SBATCH -o output.log

MPIRUN_OPTIONS="-np 24"
module load gromacs/2016.1-avx2

mpirun -n 1 gmx_mpi grompp -f md-nvt.mdp -c bithio.gro -p bithio_0.top -o bithio-md-nvt_0.tpr
mpirun $MPIRUN_OPTIONS gmx_mpi  mdrun -s bithio-md-nvt_0.tpr   -deffnm  bithio-md-nvt_0

mpirun -n 1 gmx_mpi grompp -f md-nvt.mdp -c bithio.gro -p bithio_2.top -o bithio-md-nvt_2.tpr
mpirun $MPIRUN_OPTIONS gmx_mpi  mdrun -s bithio-md-nvt_2.tpr   -deffnm  bithio-md-nvt_2
```
