import sys, getopt, math
from copy import deepcopy

class Molecule:
   def __init__(self):
       self.atoms = []
       self.rx = []
       self.ry = []
       self.rz = []
   def add(self, atom, x, y, z):
       self.atoms.append(atom)
       self.rx.append(x)
       self.ry.append(y)
       self.rz.append(z)
   def empty(self):
       return len(self.atoms) == 0
   def distance(self, mfrag):
       rdist = 1000000.0
       for x, y, z in zip(self.rx, self.ry, self.rz):
           for ax, ay, az in zip(mfrag.rx, mfrag.ry, mfrag.rz):
               abx = x - ax
               aby = y - ay
               abz = z - az
               cdist = math.sqrt(abx * abx + aby * aby + abz * abz)
               if cdist < rdist:
                   rdist = cdist
       return rdist
                   
def main(argv):
   finp_name = ''
   fout_name = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print('generator.py -i <single frame gro file> -o <VeloxChem input file>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('generator.py -i <single frame gro file> -o <VeloxChem input file>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         finp_name = arg
      elif opt in ("-o", "--ofile"):
         fout_name = arg
         
   label = None
   mlist = []
   with open(finp_name, 'r') as finp:
        mol = Molecule()
        for line in finp.readlines():
            keywords = line.split()
            if keywords[0] == 'Generated' and len(mlist) > 0:
                break
            klen = len(keywords)
            if klen == 5 or klen == 6:
                if label != keywords[0]:
                    if not mol.empty():
                        mlist.append(deepcopy(mol))
                        mol = Molecule()
                    label = keywords[0]
                    atom = (keywords[1])[0]
                    x = 10.0 * float(keywords[2 + klen - 5])
                    y = 10.0 * float(keywords[3 + klen - 5])
                    z = 10.0 * float(keywords[4 + klen - 5])
                    mol.add(atom, x, y, z)
                else:
                    atom = (keywords[1])[0]
                    x = 10.0 * float(keywords[2 + klen - 5])
                    y = 10.0 * float(keywords[3 + klen - 5])
                    z = 10.0 * float(keywords[4 + klen - 5])
                    mol.add(atom, x, y, z)
            else:
                if not mol.empty():
                    mlist.append(mol)
                    mol = Molecule()
    
   solvents = []
   for i in range(1, len(mlist)):
       if mlist[0].distance(mlist[i]) < 5.0:
           solvents.append(deepcopy(mlist[i]))

   header = """@jobs
task: response
@end

@method settings
basis: def2-svp
xcfun: b3lyp
@end

@response
property: absorption
nstates: 5
@end

@molecule
charge: 0
multiplicity: 1
xyz:
"""

   with open(fout_name, 'w') as fout:
       fout.write(header)
       mol = mlist[0]
       for lbl, x, y, z in zip (mol.atoms, mol.rx, mol.ry, mol.rz):
           fout.write(lbl + " " + "%.5f" % x + " " + "%.5f" % y + " " + "%.5f" % z + "\n")
       fout.write("@end")

   natoms = 3 * len(solvents)
   with open("tip3p.pot", 'w') as fout:
       fout.write("@COORDINATES\n" + str(natoms) + "\nAA\n")
       for mol in solvents:
           for lbl, x, y, z in zip (mol.atoms, mol.rx, mol.ry, mol.rz):
               fout.write(lbl + " " + "%.5f" % x + " " + "%.5f" % y + " " + "%.5f" % z + "\n")
       fout.write("@MULTIPOLES\nORDER 0\n" + str(natoms) + "\n")
       index = 1
       for mol in solvents:
           for lbl in mol.atoms:
               charge = "-0.834"
               if lbl == 'H':
                   charge = " 0.417"
               istr = str(index)
               fout.write(istr + (6 - len(istr)) * " " + charge + "\n")
               index = index + 1
       fout.write("@EXCLISTS\n" + str(natoms) + "  3\n")
       index = 0
       for mol in solvents:
           id0 = str(3 * index + 1)
           id1 = str(3 * index + 2)
           id2 = str(3 * index + 3)
           fout.write(id0 + " " + id1 + " " + id2 + "\n")
           fout.write(id1 + " " + id0 + " " + id2 + "\n")
           fout.write(id2 + " " + id0 + " " + id1 + "\n")
           index = index + 1
           
   with open("pol.pot", 'w') as fout:
       fout.write("@COORDINATES\n" + str(natoms) + "\nAA\n")
       for mol in solvents:
           for lbl, x, y, z in zip (mol.atoms, mol.rx, mol.ry, mol.rz):
               fout.write(lbl + " " + "%.5f" % x + " " + "%.5f" % y + " " + "%.5f" % z + "\n")
       fout.write("@MULTIPOLES\nORDER 0\n" + str(natoms) + "\n")
       index = 1
       for mol in solvents:
           for lbl in mol.atoms:
               charge = "-0.674444"
               if lbl == 'H':
                   charge = " 0.337222"
               istr = str(index)
               fout.write(istr + (6 - len(istr)) * " " + charge + "\n")
               index = index + 1
       fout.write("@POLARIZABILITIES\nORDER 1 1\n" + str(natoms) + "\n")
       index = 1
       for mol in solvents:
           for lbl in mol.atoms:
               polten = "5.73935090    0.00000000    0.00000000    5.73935090    0.00000000    5.73935090"
               if lbl == 'H':
                   polten = "2.30839051    0.00000000    0.00000000    2.30839051    0.00000000    2.30839051"
               istr = str(index)
               fout.write(istr + (6 - len(istr)) * " " + polten + "\n")
               index = index + 1
       fout.write("@EXCLISTS\n" + str(natoms) + "  3\n")
       index = 0
       for mol in solvents:
           id0 = str(3 * index + 1)
           id1 = str(3 * index + 2)
           id2 = str(3 * index + 3)
           fout.write(id0 + " " + id1 + " " + id2 + "\n")
           fout.write(id1 + " " + id0 + " " + id2 + "\n")
           fout.write(id2 + " " + id0 + " " + id1 + "\n")
           index = index + 1

if __name__ == "__main__":
   main(sys.argv[1:])
